import { initializeApp } from "firebase/app";
import { getDatabase } from '@firebase/database';

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyA9fFl1w6t_Tee9i7tYKgSewQa9WzdewzQ",
  authDomain: "facture-react.firebaseapp.com",
  projectId: "facture-react",
  storageBucket: "facture-react.appspot.com",
  messagingSenderId: "754744280195",
  appId: "1:754744280195:web:1f21ba8fa5f6be2f0e87b6",
  databaseURL: "https://facture-react-default-rtdb.firebaseio.com/" // Add your Realtime Database URL here
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);

export { app, db };
