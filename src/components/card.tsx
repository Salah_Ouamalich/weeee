import React from "react";

interface CardProps {
  invoiceNumber: string | number;
  total: string | number;
  date: string;
}

const Card: React.FC<CardProps> = ({ invoiceNumber, total , date}) => {
  return (
    <div className="bg-white  shadow rounded p-4 m-2 w-64">
      <div className="flex flex-col items-start">
        <h2 className="text-xl font-bold mb-2">Facture #{invoiceNumber}</h2>
        <p className="text-gray-600 dark:text-gray-300">Total: ${total}</p>
        <p className="text-gray-600 dark:text-gray-300">Date: {date}</p>
      </div>
    </div>
  );
};

export default Card;
