/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState } from "react";
import html2canvas from "html2canvas";
import  Article  from "../../components/truc a extract/interface";

function ModelA() {
  const [showModal, setShowModal] = useState(false);
  const [showModal2, setShowModal2] = useState(false);

  const [numeroFacture, setNumeroFacture] = useState("");
  const [dateFacture, setDateFacture] = useState("");
  const [venduA1, setVenduA1] = useState("");
  const [venduA2, setVenduA2] = useState("");
  const [venduA3, setVenduA3] = useState("");

  // const [appelService1, setAppelService1] = useState("");
  // const [appelService2, setAppelService2] = useState("");
  // const [appelService3, setAppelService3] = useState("");




  const [articles, setArticles] = useState<Article[]>([]);
  const [editingArticleId, setEditingArticleId] = useState<number | null>(null);

  const [ArticleName, setArticleName] = useState("");
  const [Description, setDescription] = useState("");
  const [Quantite, setQuantite] = useState(0);
  const [PrixUnit, setPrixUnit] = useState(0);



  function handleEditClick(articleId: number) {
    // Trouvez l'article à modifier en fonction de l'ID
    const articleToEdit = articles.find((article) => article.id === articleId);

    // Si un article a été trouvé, définissez les valeurs des champs de saisie en fonction de cet article
    if (articleToEdit) {
      setArticleName(articleToEdit.article);
      setDescription(articleToEdit.description);
      setQuantite(articleToEdit.quantite);
      setPrixUnit(articleToEdit.prixUnit);
    }

    // Définissez l'ID de l'article en cours de modification
    setEditingArticleId(articleId);

    // Ouvrez la fenêtre modale pour permettre à l'utilisateur de modifier les valeurs
    openModal2();
  }

  function handleAddArticle() {
    // Si l'ID de l'article en cours de modification est défini, mettez à jour cet article
    if (editingArticleId !== null) {
      const updatedArticles = articles.map((article) =>
        article.id === editingArticleId
          ? {
              ...article,
              article: ArticleName,
              description: Description,
              quantite: Quantite,
              prixUnit: PrixUnit,
              montant: Quantite * PrixUnit,
            }
          : article
      );
      setArticles(updatedArticles);
      console.log(updatedArticles);
      setEditingArticleId(null);
      closeModal2();
    } else {
      // Sinon, ajoutez un nouvel article
      const newArticle: Article = {
        id: Date.now(),
        article: ArticleName,
        description: Description,
        quantite: Quantite,
        prixUnit: PrixUnit,
        montant: Quantite * PrixUnit,
      };
      setArticles([...articles, newArticle]);
    }

    closeModal();
  }

  function formatNumber(number: number) {
    return new Intl.NumberFormat("en-US", {
      style: "decimal",
      minimumFractionDigits: 2,
      maximumFractionDigits: 2,
    }).format(number);
  }
  function handleDeleteArticle(id: number) {
    const newArticles = articles.filter((article) => article.id !== id);
    setArticles(newArticles);
  }

  function handleDeleteClick(event: React.MouseEvent) {
    const articleId = parseInt(
      event.currentTarget.getAttribute("data-id") || "",
      10
    );
    if (articleId) {
      handleDeleteArticle(articleId);
    }
  }

  const totalSansTaxe = articles.reduce(
    (acc, article) => acc + article.montant,
    0
  );
  const TPS = totalSansTaxe * 0.05;
  const TVQ = totalSansTaxe * 0.09975;
  const totalAvecTaxe = totalSansTaxe + TPS + TVQ;

  function closeModal(): void {
    setShowModal(false);
    setArticleName("");
    setDescription("");
    setQuantite(0);
    setPrixUnit(0);
  }

  function openModal(): void {
    setShowModal(true);
    setArticleName("");
    setDescription("");
    setQuantite(0);
    setPrixUnit(0);
  }

  function closeModal2(): void {
    setShowModal2(false);
  }

  function openModal2(): void {
    setShowModal2(true);
  }

  function toggleClickableElements(hide: any) {
    const allElements = document.querySelectorAll("*");
    const elementsWithX = Array.from(allElements).filter((element) =>
      element.className.includes("cacher")
    );

    elementsWithX.forEach((element) => {
      if (hide) {
        element.classList.add("hide-element");
      } else {
        element.classList.remove("hide-element");
      }
    });
  }

  function takeScreenshot() {
    const element: any = document.getElementById("elementToCapture");
    toggleClickableElements(true);
    html2canvas(element).then((canvas) => {
      const resizedCanvas = document.createElement("canvas");
      const ctx: any = resizedCanvas.getContext("2d");
      const scaleFactor = 0.8; // Ajustez cette valeur en fonction de vos besoins

      resizedCanvas.width = canvas.width * scaleFactor;
      resizedCanvas.height = canvas.height * scaleFactor;

      ctx.drawImage(canvas, 0, 0, resizedCanvas.width, resizedCanvas.height);

      // Utilisez le canvas redimensionné (resizedCanvas) pour générer l'image
      const dataUrl = resizedCanvas.toDataURL("image/png");
      const link = document.createElement("a");
      link.href = dataUrl;
      link.download = `facture-${numeroFacture}.png`;
      link.click();
    });
    toggleClickableElements(false);
  }

  return (
    <div className="w-full flex justify-center  ">
      <div className="w-1/2  " id="elementToCapture">
        <div className=" bg-black pb-3">
          <h1 className="text-white text-2xl font-bold w-full text-center ">
            F A C T U R E{" "}
          </h1>
        </div>

        <div className="flex   mt-5">
          <div className="flex-col w-full">
            <h2 className="font-bold text-base w-fit underline">
              RSA DIESEL - SERVICE ROUTIER
            </h2>
            <h2 className="text-base w-fit">2-2551 Hogan H2K 2T5</h2>
            <h2 className="text-base">Tel:(438)-933-6562</h2>
            <h2 className="text-base">rsadiesel@gmail.com</h2>
          </div>
          <div className="w-1/2 ">
            <div className="flex-col  ">
              <div className="flex justify-end mb-8">
                <h2 className="text-base ">Numéro : </h2>
                <input
                  className=" w-16 border-1 border-black rounded-md text-base text-end"
                  type="text"
                  value={numeroFacture}
                  onChange={(e) => setNumeroFacture(e.target.value)}
                />

              </div>
              <div className="flex justify-end ">
                <h2 className="text-base ">Date : </h2>
                <input
                  className=" w-24 border-1 border-black rounded-md text-base text-end"
                  type="text"
                  value={dateFacture}
                  onChange={(e) => setDateFacture(e.target.value)}
                />
                
              </div>       
              <div className="flex justify-end ">
              <input
                  className=" w-30 border-1 border-black rounded-md text-base text-end"
                  type="text"
                />   
                </div>
            </div>
          </div>
        </div>

        <div className=" flex mt-5 w-full  ">
          <div className="flex flex-col w-1/2   ">
            <h2 className="text-base font-bold w-fit"> Vendu à : </h2>
            <input
              className="w-44 border- mt-2 border-black rounded-md text-base"
              type="text"
              value={venduA1}
              onChange={(e) => setVenduA1(e.target.value)}
            />
            <input
              className="w-44 border-1 border-black rounded-md text-base"
              type="text"
              value={venduA2}
              onChange={(e) => setVenduA2(e.target.value)}
            />
            <input
              className="w-44 border-1 border-black rounded-md text-base"
              type="text"
              value={venduA3}
              onChange={(e) => setVenduA3(e.target.value)}

            />
          </div>
          <div className="flex  w-1/2    ">
            <div className="flex-col  w-full  ">
              <div className="flex justify-end">
                <h2 className="text-base font-bold w-fit ">
                  {" "}
                  Appelle de service à :{" "}
                </h2>
              </div>
              <div className="mt-2">
              <div className="flex justify-end ">
                <input
                  className="w-44 border-1  border-black rounded-md text-base"
                  type="text"
                  // value={appelService}
                  // onChange={(e) => setAppelService1(e.target.value)}
                />
              </div>
              <div className="flex justify-end ">
                <input
                  className="w-44 border-1  border-black rounded-md text-base"
                  type="text"
                  // value={appelService2}
                  // onChange={(e) => setAppelService2(e.target.value)}
                />
              </div>
              <div className="flex justify-end ">
                <input
                  className="w-44 brder-1  border-black rounded-md text-base"
                  type="text"
                  // value={appelService3}
                  // onChange={(e) => setAppelService3(e.target.value)}
                />
              </div>
              </div>
            </div>
          </div>
        </div>

        <div className="w-full">
          <div className="flex justify-center mt-8 ">
            <table className="inventory">
              <thead>
                <tr>
                  <th>
                    <span className="">Article</span>
                  </th>
                  <th colSpan={5}>
                    <span>Description</span>
                  </th>
                  <th className=" w-28">
                    <span>Quantité/Heure</span>
                  </th>
                  <th className=" w-16">
                    <span>Prix Unit</span>
                  </th>
                  <th className=" w-16">
                    <span>Montant</span>
                  </th>
                </tr>
              </thead>
              <tbody>
                {articles.map((article) => (
                  <tr key={article.id}>
                    <td>
                      <div className="relative ">
                        <div className="absolute top-0 cursor-pointer mb-5  left-[-30px] bg-blue-500 w-5 h-5 rounded-full flex items-center justify-center cacher ">
                          <p
                            className=" p-0 m-0  text-white mb-1 text-lg rounded-full mb"
                            data-id={article.id}
                            onClick={handleDeleteClick}
                          >
                            -
                          </p>
                        </div>
                        <div className="absolute top-0 cursor-pointer mb-4  left-[-60px] w-5 h-5 rounded-full flex items-center justify-center cacher ">
                          <p
                            className=" p-0 m-0  text-white mb-1 text-sm rounded-full mb"
                            data-id={article.id}
                            onClick={() => handleEditClick(article.id)}
                          >
                            ✏️
                          </p>
                        </div>
                      </div>
                      <div className="nextline text-sm ">{article.article}</div>
                    </td>
                    <td colSpan={5}>
  <div className="nextline text-sm" style={{ whiteSpace: 'pre-wrap' }}>
    {article.description}
  </div>
</td>

                    <td colSpan={1}>
                      <div className="nextline text-sx ">
                        {article.quantite}
                      </div>
                    </td>
                    <td>
                      <span data-prefix>$ </span>
                      <span className="text-sx ">
                        {formatNumber(article.prixUnit)}
                      </span>
                    </td>
                    <td>
                      <span data-prefix>$</span>
                      <span className="text-sx ">
                        {formatNumber(article.montant)}
                      </span>
                    </td>
                  </tr>
                ))}

                {/* fin */}
              </tbody>
              <div className="bg-blue-500 w-5 h-5 rounded-full mt-1 cursor-pointer flex items-center justify-center cacher">
                <p
                  className=" p-0 m-0 text-white mb-1 text-lg rounded-full mb"
                  onClick={openModal}
                >
                  +
                </p>
              </div>
            </table>
          </div>
        </div>

        {/* tableau de droite */}
        <div className="w-full flex justify-end items-end ">
          <div className=" w-64 ">
            <div className=" ">
              <table className="balance">
                <tr>
                  <th>
                    <span>Montant hors taxes</span>
                  </th>
                  <td>
                    <span data-prefix>$ </span>
                    <span className="text-sm ">
                      {formatNumber(totalSansTaxe)}
                    </span>
                  </td>
                </tr>
                <tr>
                  <th>
                    <span>TPS(5%)</span>
                  </th>
                  <td>
                    <span data-prefix>$ </span>
                    <span className="text-sm ">{formatNumber(TPS)}</span>
                  </td>
                </tr>
                <tr>
                  <th>
                    <span>TVQ(9.975%)</span>
                  </th>
                  <td>
                    <span data-prefix>$ </span>
                    <span className="text-sm ">{formatNumber(TVQ)}</span>
                  </td>
                </tr>
                <tr>
                  <th>
                    <span>Montant avec taxes</span>
                  </th>
                  <td>
                    <span data-prefix>$ </span>
                    <span className="text-sm ">
                      {formatNumber(totalAvecTaxe)}
                    </span>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
        {/* fin tableau de droite */}

        <div className="flex justify-center text-center flex-col mt-7">
          <p className="text-lg "> R E M A R Q U E S </p>
          <div className="bg-black w-full h-[0.2px]"></div>
        </div>
        <div className="mt-3">
          <p className="">PAYABLE À L'ORDRE DE RSA DIESEL SERVICE ROUTIER</p>
          <br></br>
          <p>Numero de TPS/TVH: #751509407RT0001</p>
          <p>Numero de TVQ: #4010903144TQ0001</p>

          <button onClick={takeScreenshot} className="cacher">
            Prendre un screenshot
          </button>
        </div>

        {/*modal ajouter */}
        {showModal && (
          <div className="fixed inset-0 bg-black bg-opacity-30 backdrop-blur-sm flex justify-center items-center">
            <div className="w-1/2 h-1/2 flex flex-col bg-white">
              <button
                className="bg-red-500 w-10 h-10 rounded-full text-white place-self-end m-2"
                onClick={closeModal}
              >
                X
              </button>
              <h2 className="text-2xl text-center">Ajouter un article </h2>
              <div className="flex justify-center">
                <div className="flex flex-col w-1/2">
                  <label htmlFor="article">Article</label>
                  {/* Article input */}
                  <input
                    className="border border-black rounded-md text-base"
                    type="text"
                    placeholder="nom de l'article"
                    value={ArticleName}
                    onChange={(e) => setArticleName(e.target.value)}
                  />

                  <label htmlFor="description">Description</label>
                  {/* Description input */}
                  <textarea
                    className="border border-black rounded-md text-base"
                    placeholder="description de l'article"
                    value={Description}
                    onChange={(e) => setDescription(e.target.value)}
                  />

                  <label htmlFor="quantite">Quantité/Heure</label>
                  {/* Quantite/Heure input */}
                  <input
                    className="border border-black rounded-md text-base"
                    type="number"
                    step="0.01"
                    placeholder=" Quantité/Heure"
                    value={Quantite}
                    onChange={(e) => setQuantite(Number(e.target.value))}
                  />

                  <label htmlFor="prix">Prix Unit</label>
                  {/* Prix Unit input */}
                  <input
                    className="border border-black rounded-md text-base"
                    type="number"
                    step="0.01"
                    placeholder="Prix Unit"
                    value={PrixUnit}
                    onChange={(e) => setPrixUnit(Number(e.target.value))}
                  />
                  <div className="flex justify-center mt-4">
                    <button
                      className="bg-blue-500 w-20 h-10 rounded-md text-white"
                      onClick={handleAddArticle}
                    >
                      Ajouter
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
        {/* fin modal */}

        {/* modal modifier */}
        {showModal2 && (
          <div className="fixed inset-0 bg-black bg-opacity-30 backdrop-blur-sm flex justify-center items-center">
            <div className="w-1/2 h-1/2 flex flex-col bg-white">
              <button
                className="bg-red-500 w-10 h-10 rounded-full text-white place-self-end m-2"
                onClick={closeModal2}
              >
                X
              </button>
              <h2 className="text-2xl text-center">Modifier un article </h2>
              <div className="flex justify-center">
                <div className="flex flex-col w-1/2">
                  <label htmlFor="article">Article</label>
                  {/* Article input */}
                  <input
                    className="border border-black rounded-md text-base"
                    type="text"
                    placeholder="nom de l'article"
                    value={ArticleName}
                    onChange={(e) => setArticleName(e.target.value)}
                  />

                  <label htmlFor="description">Description</label>
                  {/* Description input */}
                  <textarea
                    className="border border-black rounded-md text-base"
                    placeholder="description de l'article"
                    value={Description}
                    onChange={(e) => setDescription(e.target.value)}
                  />

                  <label htmlFor="quantite">Quantité/Heure</label>
                  {/* Quantite/Heure input */}
                  <input
                    className="border border-black rounded-md text-base"
                    type="number"
                    step="0.01"
                    placeholder=" Quantité/Heure"
                    value={Quantite}
                    onChange={(e) => setQuantite(Number(e.target.value))}
                  />

                  <label htmlFor="prix">Prix Unit</label>
                  {/* Prix Unit input */}
                  <input
                    className="border border-black rounded-md text-base"
                    type="number"
                    step="0.01"
                    placeholder="Prix Unit"
                    value={PrixUnit}
                    onChange={(e) => setPrixUnit(Number(e.target.value))}
                  />
                  <div className="flex justify-center mt-4">
                    <button
                      className="bg-red-500 w-20 h-10 rounded-md text-white ml-4"
                      onClick={handleAddArticle}
                    >
                      Modifier
                    </button>
                  </div>
                </div>
              </div>

              {/* fin modal */}
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default ModelA;
