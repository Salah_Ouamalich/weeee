import React, { useEffect, useState } from "react";
import Cookies from "js-cookie";
import Card from "../components/card";
import SearchBar from "../components/searchBar";
import { useNavigate } from "react-router-dom";


function Home() {
  const [name, setName] = useState("");
  const [invoices, setInvoices] = useState([
    // Add your invoice data here, for example:
    // { id: "1", month: "January", total: 100 },
    // { id: "2", month: "February", total: 200 },
  ]);

  useEffect(() => {
    const displayName = Cookies.get("displayName");
    if (displayName) {
      setName(displayName);
    }
  }, []);


  const navigate = useNavigate();

  const handleSearch = (searchTerm: any) => {
    console.log("Searching for:", searchTerm);
    // Implement the search logic here
  };

  return (
    <div>
      <div className="p-4 sm:ml-64">
        <div className="p-4 border-2 border-gray-200 border-dashed rounded-lg mt-14">
          <div className="mb-4">
            <p className="text-2xl text-black ">Bonjour {name} 😁</p>
          </div>
          <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" onClick={() => navigate("/model-a")}>
            Ajouter une facture
          </button>
          <div className="flex items-center justify-center h-48 mb-4 rounded">
            <p className="text-2xl text-gray-400 dark:text-gray-500">..... </p>
          </div>


          <div className="mb-4">
            <p className="text-2xl text-black ">Mes factures</p>
          </div>
          <div className="mb-4">
            <SearchBar onSearch={handleSearch} />
          </div>
          
          <div className="grid grid-cols-5 gap-4">
            {/* {invoices.map((invoice) => (
              <Card key={invoice.id} invoiceNumber={invoice.id} identifier={invoice.identifier} />))} */}

<div className="bg-white  shadow rounded p-4 m-2 w-64 cursor-pointer scale-100 hover:scale-105 transition duration-300"> 
              <div className="flex flex-col items-start">
                <h2 className="text-xl font-bold mb-2">Facture #3333</h2>
                <p className="text-gray-600 text-base ">Total: 1000$</p>
                <p className="text-gray-500 text-base mt-2 ">Date: 01/01/2021</p>
              </div>
            </div> 
          </div>
        </div>
      </div>
    </div>
  );
}

export default Home;
