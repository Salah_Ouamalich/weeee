import React from "react";
import { getAuth, signInWithPopup, GoogleAuthProvider, getAdditionalUserInfo, User } from 'firebase/auth';
import { getDatabase, ref, set } from '@firebase/database';
import Cookies from 'js-cookie';
import { app } from '../firebaseconf';
import { useNavigate } from "react-router-dom";


const Login: React.FC = () => {
  const auth = getAuth(app);
  const provider = new GoogleAuthProvider();
  const navigate = useNavigate();

  const createUserEntry =  (user: User) => {
    const db = getDatabase(app);
    const userRef = ref(db, `users/${user.uid}`);
    set(userRef, {
      uid: user.uid,
      facture: [],
    });
  };
    
  const signInWithGoogle = async () => {
    try {
      const result = await signInWithPopup(auth, provider);
      const user = result.user;
      console.log('User logged in:', user);

      if (user) {
        Cookies.set('email', user.email ?? '');
        Cookies.set('displayName', user.displayName ?? '');
        

        // Check if the user is new
        const additionalUserInfo = getAdditionalUserInfo(result);
        if (additionalUserInfo?.isNewUser) {
          // Create user entry in the Realtime Database
          createUserEntry(user);
          console.log('User entry created in the Realtime Database');
        }
        navigate('/');
      }
    } catch (error) {
      console.error('Error signing in with Google:', error);
    }
  };


    return (
      <div className="min-h-screen bg-white dark:bg-gray-800 flex items-center justify-center">
        <div className="bg-white dark:bg-gray-700 p-8 rounded-lg shadow-md w-80 flex items-center justify-center">
          <div className="flex items-center justify-center ">
            <div className="flex w-full  border rounded-2xl p-3 cursor-pointer hover:bg-gray-100 dark:hover:bg-gray-600">
                <img src="https://img.icons8.com/color/48/000000/google-logo.png" alt="google" className="h-5 mr-2" />
                <p className="text-white  text-sm font-semibold" onClick={signInWithGoogle}>Sign in with Google</p> 
                </div>
          </div>
        </div>
      </div>
    );
  };
  
  export default Login;
  